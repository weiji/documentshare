package net.documentshare.ftp;

import java.io.Serializable;

public class FtpConfig implements Serializable {
	private static final long serialVersionUID = -2896267214283891928L;
	public final String host;
	public final int port;
	public final String user, pass;

	public boolean validateFileSize = false;

	public final String path, name;

	public FtpConfig(final String host, final int port, final String user, final String pass, final String path) {
		this(host, port, user, pass, path, null);
	}

	public FtpConfig(final String host, final int port, final String user, final String pass, final String path, final String name) {
		this.host = host;
		this.port = port;
		this.user = user;
		this.pass = pass;
		this.path = path;
		this.name = name;
	}

}
