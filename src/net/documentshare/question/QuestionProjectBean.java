package net.documentshare.question;

import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.core.id.ID;

/**
 * 关联的开源项目
 * @author lg
 *
 */
public class QuestionProjectBean extends AbstractIdDataObjectBean {
	private ID questionId;//类别
	private ID projectId;// 关联到的开源项目

	public QuestionProjectBean() {
	}

	public void setQuestionId(ID questionId) {
		this.questionId = questionId;
	}
	
	public ID getQuestionId() {
		return questionId;
	}

	public void setProjectId(ID projectId) {
		this.projectId = projectId;
	}

	public ID getProjectId() {
		return projectId;
	}

}
