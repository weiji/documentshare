package net.documentshare.question.remark;

import net.documentshare.i.IItSiteApplicationModule;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.web.page.component.ComponentParameter;

public interface IQuestionRemarkApplicationModule extends IItSiteApplicationModule {

	IDataObjectQuery<QuestionRemarkItem> getRemarkItems(ComponentParameter compParameter);

	IDataObjectQuery<QuestionRemarkItem> getRemarkItems(ComponentParameter compParameter, Object parentId);

	QuestionRemarkItem doSupportOpposition(ComponentParameter compParameter, Object itemId, boolean support);

	void doRemarkSent(final ComponentParameter compParameter, final QuestionRemarkItem item);
}
