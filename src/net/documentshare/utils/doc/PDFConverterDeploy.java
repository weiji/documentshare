package net.documentshare.utils.doc;

import java.io.File;


public class PDFConverterDeploy {
    private File outputDirectory;
    private int pageCount;
    private String command;

    public File getOutputDirectory() {
        return outputDirectory;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String getCommand() {
        return command;
    }

    public PDFConverterDeploy(File outputDirectory, int pageCount, String command) {
        this.outputDirectory = outputDirectory;
        this.pageCount = pageCount;
        this.command = command;
    }
}
