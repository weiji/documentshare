package net.documentshare;

import net.simpleframework.my.home.DefaultHomeApplicationModule;
import net.simpleframework.my.home.HomeTabsBean;
import net.simpleframework.web.page.PageRequestResponse;

public class DShareHomeApplicationModule extends DefaultHomeApplicationModule {

	@Override
	public String getTabUrl(final PageRequestResponse requestResponse, final HomeTabsBean homeTab) {
		final StringBuilder sb = new StringBuilder();
		sb.append("/home/").append(homeTab.getId()).append(".html");
		return sb.toString();
	}
}
