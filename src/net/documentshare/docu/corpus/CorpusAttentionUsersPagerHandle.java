package net.documentshare.docu.corpus;

import java.util.Map;

import net.documentshare.docu.DocuUtils;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.util.StringUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractPagerHandle;

public class CorpusAttentionUsersPagerHandle extends AbstractPagerHandle {
	@Override
	public Map<String, Object> getFormParameters(final ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "corpusId");
		return parameters;
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final String docuId = compParameter.getRequestParameter("corpusId");
		if (StringUtils.hasText(docuId)) {
			final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager(CorpusAttention.class);
			return tMgr.query(new ExpressionValue("corpusId=?", new Object[] { docuId }), CorpusAttention.class);
		} else {
			return null;
		}
	}
}
