package net.documentshare.docu.all;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import net.documentshare.docu.DocuBean;
import net.documentshare.docu.DocuUtils;
import net.documentshare.docu.EDocuStatus;
import net.documentshare.utils.ItSiteUtil;
import net.simpleframework.ado.db.ExpressionValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.content.ContentUtils;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.organization.IJob;
import net.simpleframework.util.ConvertUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;
import net.simpleframework.web.page.component.ui.pager.db.AbstractDbTablePagerHandle;

public class AllDocuTablePaperNonHandle extends AbstractDbTablePagerHandle {
	@Override
	public Object getBeanProperty(ComponentParameter compParameter, String beanProperty) {
		if ("jobExecute".equals(beanProperty)) {
			return IJob.sj_account_normal;
		}
		return super.getBeanProperty(compParameter, beanProperty);
	}

	@Override
	public IDataObjectQuery<?> createDataObjectQuery(final ComponentParameter compParameter) {
		final ITableEntityManager tMgr = DocuUtils.applicationModule.getDataObjectManager();
		final List<Object> ol = new ArrayList<Object>();
		final StringBuffer sql = new StringBuffer();
		final StringBuffer order = new StringBuffer();
		final String docu_status = compParameter.getRequestParameter("docu_status");
		sql.append("status=?");
		try {
			ol.add(EDocuStatus.valueOf(docu_status));
		} catch (Exception e) {
			ol.add(EDocuStatus.publish);
		}
		if (!ItSiteUtil.isManage(compParameter, DocuUtils.applicationModule)) {
			sql.append(" and userId=?");
			ol.add(ItSiteUtil.getLoginUser(compParameter).getId());
		}
		order.append("createDate desc");
		sql.append(" order by ").append(order);
		return tMgr.query(new ExpressionValue(sql.toString(), ol.toArray(new Object[] {})), DocuBean.class);
	}

	@Override
	public AbstractTablePagerData createTablePagerData(final ComponentParameter compParameter) {
		return new AbstractTablePagerData(compParameter) {

			@Override
			protected Map<Object, Object> getRowData(final Object arg0) {
				final DocuBean docuBean = (DocuBean) arg0;
				final Map<Object, Object> rowData = new LinkedHashMap<Object, Object>();
				final StringBuffer buf = new StringBuffer();
				buf.append(DocuUtils.wrapOpenLink(compParameter, docuBean));
				rowData.put("title", buf.toString());
				rowData.put("userId", ContentUtils.getAccountAware().wrapAccountHref(compParameter, docuBean.getUserId(), docuBean.getUserText()));
				rowData.put("createDate", ConvertUtils.toDateString(docuBean.getCreateDate(), "yyyy-MM-dd HH"));
				rowData.put("docuFunction", docuBean.getDocuFunction());
				rowData.put("success", docuBean.getSuccessStatus());
				rowData.put("action", "<a class='allDB down_menu_image'></a>");
				return rowData;
			}
		};
	}

	@Override
	public Map<String, Object> getFormParameters(ComponentParameter compParameter) {
		final Map<String, Object> parameters = super.getFormParameters(compParameter);
		putParameter(compParameter, parameters, "docu_status");
		return parameters;
	}

}
