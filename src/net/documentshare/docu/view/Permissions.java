package net.documentshare.docu.view;

public enum Permissions {
	NONE, READ_ONLY, READ_AND_COPY, READ_ANNOTATIONS, WRITE_ANNOTATIONS, ALL
}
