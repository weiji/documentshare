package net.documentshare.docu;

import net.documentshare.impl.AbstractAttention;
import net.simpleframework.core.id.ID;

public class DocuAttention extends AbstractAttention {
	private ID docuId;

	public void setDocuId(ID docuId) {
		this.docuId = docuId;
	}

	public ID getDocuId() {
		return docuId;
	}

}
