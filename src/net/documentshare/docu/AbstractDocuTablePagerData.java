package net.documentshare.docu;

import net.simpleframework.content.EContentType;
import net.simpleframework.util.IoUtils;
import net.simpleframework.web.page.component.ComponentParameter;
import net.simpleframework.web.page.component.ui.pager.AbstractTablePagerData;

public abstract class AbstractDocuTablePagerData extends AbstractTablePagerData {
	public AbstractDocuTablePagerData(final ComponentParameter compParameter) {
		super(compParameter);
	}

	public String buildTitle(final DocuBean docuBean) {
		final StringBuffer sb = new StringBuffer();
		sb.append("<table><tr>");
		sb.append("<td valign='top'>");
		sb.append("<img class='docuPage' src='" + DocuUtils.getPageImgSrc(compParameter, docuBean) + "'>");
		sb.append("</td>");
		sb.append("<td valign='top' width=\"100%\">");
		sb.append("<table>");
		sb.append("<tr><td>");
		sb.append("<div class=\"f4\" style=\"margin: 4px 0px;\">");
		sb.append(DocuUtils.wrapOpenLink(compParameter, docuBean));
		sb.append("</div>");
		sb.append("</td></tr>");
		sb.append("<tr><td valign=\"top\">");
		sb.append("<table>");
		sb.append("<tr>");
		sb.append("<td align=\"right\">");
		sb.append("阅读:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append(wrapNum(docuBean.getViews()));
		sb.append("</td>");
		sb.append("<td align=\"right\">");
		sb.append("关注:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append(wrapNum(docuBean.getAttentions()));
		if (docuBean.getTtype() == EContentType.recommended) {
			sb.append("<img style='margin-left: 5px;' src='/simple/common/images/recommended.gif'/>");
		}
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("<tr>");
		sb.append("<td align=\"right\">");
		sb.append("下载:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append(wrapNum(docuBean.getDownCounter()));
		sb.append("</td>");
		sb.append("<td align=\"right\">");
		sb.append("评论:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append(wrapNum(docuBean.getRemarks()));
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("<tr>");
		sb.append("<td align=\"right\">");
		sb.append("页数:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append(wrapNum(docuBean.getFileNum()));
		sb.append("</td>");
		sb.append("<td align=\"right\">");
		sb.append("大小:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append(wrapNum(IoUtils.toFileSize(docuBean.getFileSize())));
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("<tr>");
		sb.append("<td align=\"right\">");
		sb.append("类型:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append("<img alt='" + docuBean.getExtension() + "' src='" + DocuUtils.getFileImage(compParameter, docuBean) + "');'></span>");
		sb.append("</td>");
		sb.append("<td align=\"right\">");
		sb.append("评分:");
		sb.append("</td>");
		sb.append("<td>");
		sb.append("<span title='" + docuBean.getTotalGrade() + "' class='m_star m_" + docuBean.getStarGrade() + "'></span>");
		sb.append("</td>");
		sb.append("</tr>");
		sb.append("</table>");
		sb.append("</td>");
		sb.append("</tr></table>");
		sb.append("</td></tr></table>");
		return sb.toString();
	}

	protected String wrapNum(final Object num) {
		final StringBuilder sb = new StringBuilder();
		sb.append("<span class=\"nnum\">").append(num).append("</span>");
		return sb.toString();
	}
}
