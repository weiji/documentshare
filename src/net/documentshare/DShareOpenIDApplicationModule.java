package net.documentshare;

import net.simpleframework.applets.openid.DefaultOpenIDApplicationModule;
import net.simpleframework.web.page.PageRequestResponse;

public class DShareOpenIDApplicationModule extends DefaultOpenIDApplicationModule {

	@Override
	protected String getReturnToURL(final PageRequestResponse requestResponse) {
		final StringBuilder url = new StringBuilder();
		url.append(getApplication().getApplicationConfig().getServerUrl());
		url.append(requestResponse.wrapContextPath("/openid.html"));
		return url.toString();
	}
}
