package net.documentshare.documentconfig;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.Map;

import net.documentshare.docu.DocuBean;
import net.documentshare.docu.DocuUtils;
import net.documentshare.utils.IOUtils;
import net.simpleframework.util.IoUtils;

import org.dom4j.Element;

import com.itextpdf.text.pdf.PdfReader;

/**
 * 本地存储
 *
 */
public class LocalStorageBean extends StorageBean {

	public LocalStorageBean(Element ele) {
		super(ele);
	}

	@Override
	protected void initStorage(Element ele) {
		super.initStorage(ele);
	}

	@Override
	public String getType() {
		return "local";
	}

	@Override
	public boolean canDelete() {
		final File file = new File(path);
		if (!file.exists()) {
			return true;
		}
		return file.list().length == 0;
	}

	@Override
	public boolean hasFreeSpace() {
		return new File(path).getFreeSpace() / 1024 / 1024 > diskSpace;
	}

	@Override
	public void convertDocument(DocuBean docuBean) {
		final String path1 = path + DocuUtils.getDatabase(docuBean.getUserId());
		if (!new File(path1 + docuBean.getId()).exists()) {
			DocuUtils.convertFile(path1, docuBean);
		}
	}

	@Override
	public InputStream getInputStream(String path) {
		try {
			return new FileInputStream(new File(this.path + path));
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public Map<String, Boolean> listFiles(String filePath) {
		final Map<String, Boolean> fileMap = new LinkedHashMap<String, Boolean>();
		new File(path + filePath).listFiles(new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				if (pathname.isDirectory()) {
					fileMap.put(pathname.getName(), true);
				} else {
					fileMap.put(pathname.getName(), false);
				}
				return false;
			}
		});
		return fileMap;
	}

	@Override
	public InputStream getInputStream(final DocuBean docuBean, final String path) {
		try {
			return new FileInputStream(new File(this.path + DocuUtils.getDatabase(docuBean.getUserId()) + docuBean.getId() + File.separator + path));
		} catch (Exception e) {
		}
		return null;
	}

	@Override
	public boolean isFileExists(final String filePath, final String fileName) {
		final File file = new File(path + File.separator + filePath + File.separator + fileName);
		if (!file.exists()) {
			file.getParentFile().mkdirs();
		}
		return file.exists();
	}

	@Override
	public void deleteFile(String filePath, String fileName) {
		IOUtils.delete(path + filePath + fileName);
	}

	@Override
	public int getPageCounter(String filePath) {
		try {
			return new PdfReader(path + filePath).getNumberOfPages();
		} catch (IOException e) {
		}
		return 0;
	}

	@Override
	public void uploadFile(InputStream is, final String filePath, final String fileName) {
		try {
			IoUtils.copyFile(is, new File(path + File.separator + filePath + File.separator + fileName));
		} catch (IOException e) {
		}
	}

	@Override
	public boolean isDirectoryExists() {
		new File(path).mkdirs();
		return new File(path).exists();
	}

	@Override
	public String toString() {
		final StringBuffer buf = new StringBuffer();
		buf.append("路径：").append(path).append("<br/>");
		return buf.toString();
	}

	@Override
	public String getFileNames(final String filePath) {
		final StringBuffer buf = new StringBuffer();
		IOUtils.listAllFiles(new File(this.path + filePath), new FileFilter() {

			@Override
			public boolean accept(File pathname) {
				if (pathname.isFile()) {
					buf.append(pathname.getName());
				}
				return false;
			}
		});
		return buf.toString();
	}
}
