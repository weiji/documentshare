package net.documentshare.documentconfig;

import java.io.InputStream;
import java.util.Map;

import net.documentshare.docu.DocuBean;

import org.dom4j.Element;

public abstract class StorageBean {
	protected String path;
	protected String id;
	protected Element ele;
	protected int diskSpace = 1024;

	public StorageBean(final Element ele) {
		this.ele = ele;
		if (ele != null) {
			this.path = ele.attributeValue("path");
			this.id = ele.attributeValue("id");
			initStorage(ele);
		}
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
		this.ele.addAttribute("path", path);
	}

	public Element getEle() {
		return ele;
	}

	protected void initStorage(final Element ele) {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
		this.ele.addAttribute("id", id);
	}

	public abstract String getType();

	public boolean isDirectoryExists() {
		return true;
	}

	public abstract boolean isFileExists(final String filePath, final String fileName);

	public abstract boolean canDelete();

	public abstract void deleteFile(final String filePath, final String fileName);

	public abstract void uploadFile(final InputStream is, final String filePath, final String fileName);

	public abstract boolean hasFreeSpace();

	public abstract void convertDocument(final DocuBean docuBean);

	public abstract InputStream getInputStream(final DocuBean docuBean, final String path);

	public abstract InputStream getInputStream(final String path);

	public abstract Map<String, Boolean> listFiles(final String filePath);

	public abstract int getPageCounter(final String filePath);

	public abstract String getFileNames(final String filePath);

	@Override
	public String toString() {
		return super.toString();
	}
}
