package net.documentshare.common;

public enum ECommonType {
	notify, docu_auto() {
		@Override
		public String toString() {
			return "是否自动审核";
		}
	},
	converNumber(){
		@Override
		public String toString() {
			return "转化页数";
		}
	}
}
