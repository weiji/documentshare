package net.documentshare.impl;

import java.util.Date;
import java.util.Map;

import net.documentshare.i.IAttentionBeanAware;
import net.documentshare.i.IItSiteApplicationModule;
import net.simpleframework.ado.IDataObjectValue;
import net.simpleframework.ado.db.ITableEntityManager;
import net.simpleframework.ado.db.event.TableEntityAdapter;
import net.simpleframework.core.bean.AbstractIdDataObjectBean;
import net.simpleframework.core.id.ID;
import net.simpleframework.organization.account.AccountSession;
import net.simpleframework.organization.account.IAccount;
import net.simpleframework.util.BeanUtils;
import net.simpleframework.web.page.PageRequestResponse;

public abstract class AbstractAttention extends AbstractIdDataObjectBean {
	private ID userId;
	private Date createDate;

	public AbstractAttention() {
		this.createDate = new Date();
	}

	public ID getUserId() {
		return userId;
	}

	public void setUserId(final ID userId) {
		this.userId = userId;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(final Date createDate) {
		this.createDate = createDate;
	}

	public static class Utils {
		/**
		* 更新关注
		*/
		public static void updateAttention(final String field, final PageRequestResponse requestResponse,
				final Class<? extends IAttentionBeanAware> beanClass, final Class<? extends AbstractAttention> attentionClass,
				final IItSiteApplicationModule application, final Map<String, Object> json) {
			final IAccount account = AccountSession.getLogin(requestResponse.getSession());
			if (account == null)
				return;
			synchronized (application) {
				final String id = requestResponse.getRequestParameter(field);
				final AbstractAttention attention = application.getAttention(account.getId(), id);
				if (attention == null) {
					try {
						final AbstractAttention attention1 = attentionClass.newInstance();
						attention1.setUserId(account.getId());
						BeanUtils.setProperty(attention1, field, id);
						application.doUpdate(attention1, new TableEntityAdapter() {
							@Override
							public void afterInsert(ITableEntityManager manager, Object[] objects) {
								final IAttentionBeanAware aBean = application.getBean(beanClass, id);
								aBean.setAttentions(aBean.getAttentions() + 1);
								application.doUpdate(new Object[] { "attentions" }, aBean);
								json.put("act", "add");
							}
						});
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					application.doDelete(attention, new TableEntityAdapter() {
						@Override
						public void afterDelete(ITableEntityManager manager, IDataObjectValue dataObjectValue) {
							final IAttentionBeanAware aBean = application.getBean(beanClass, BeanUtils.getProperty(attention, field));
							aBean.setAttentions(aBean.getAttentions() - 1);
							application.doUpdate(new Object[] { "attentions" }, aBean);
							json.put("act", "del");
						}
					});
				}
			}
		}
	}
}
