package net.simpleframework.core.bean;

import net.simpleframework.core.id.ID;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
@SuppressWarnings("serial")
public abstract class AbstractIdDataObjectBean extends AbstractDataObjectBean implements
		IIdBeanAware {
	private ID id;

	@Override
	public ID getId() {
		return id;
	}

	@Override
	public void setId(final ID id) {
		this.id = id;
	}

	@Override
	public boolean equals2(final IIdBeanAware idBean) {
		return idBean != null && idBean.getId().equals2(getId());
	}
}
