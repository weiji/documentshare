package net.simpleframework.swing;

import java.awt.Color;
import java.util.Map;
import java.util.Random;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import net.simpleframework.ado.DataObjectManagerFactory;
import net.simpleframework.ado.db.AbstractQueryEntitySet;
import net.simpleframework.ado.db.SQLValue;
import net.simpleframework.core.ado.DataObjectQueryAdapter;
import net.simpleframework.core.ado.IDataObjectQuery;
import net.simpleframework.swing.JActions.ActionCallback;
import net.simpleframework.util.ConvertUtils;

import org.apache.commons.dbcp.BasicDataSource;
import org.jdesktop.swingx.JXFrame.StartPosition;

public class Test {
	static BasicDataSource dataSource;

	static {
		for (final String key : new String[] { "Component.font", "TextField.font", "Button.font",
				"ToggleButton.font", "RadioButton.font", "CheckBox.font", "ColorChooser.font",
				"ComboBox.font", "Label.font", "List.font", "MenuBar.font", "MenuItem.font",
				"RadioButtonMenuItem.font", "CheckBoxMenuItem.font", "Menu.font", "PopupMenu.font",
				"OptionPane.font", "Panel.font", "ProgressBar.font", "ScrollPane.font",
				"Viewport.font", "TabbedPane.font", "Table.font", "TableHeader.font",
				"TitledBorder.font", "ToolBar.font", "Tree.font" }) {
			UIManager.getDefaults().put(key, SwingUtils.defautFont);
		}
		dataSource = new BasicDataSource();
		dataSource.setDriverClassName("org.gjt.mm.mysql.Driver");
		dataSource
				.setUrl("jdbc:mysql://127.0.0.1:3306/simple3?useUnicode=true&characterEncoding=utf-8");
		dataSource.setUsername("root");
		dataSource.setPassword("920709");

		// oracle
		// dataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
		// dataSource.setUrl("jdbc:oracle:thin:@192.168.0.111:1521:NO");
		// dataSource.setUsername("NOV3");
		// dataSource.setPassword("NOV3");

		dataSource.setTestWhileIdle(true);
		dataSource.setTimeBetweenEvictionRunsMillis(14400000);

		// dataSource
		// .setUrl("jdbc:mysql://192.168.0.111:3305/rosensetools?useUnicode=true&characterEncoding=utf-8");
		// dataSource.setUsername("root");
		// dataSource.setPassword("root");
		// final Table table = new Table("TBL_GSM_CELL_HANDOVER", "CI");
		// final Table table = new Table("tbl_gsm_s9_asc_bts", "CI");
		//
		// entityManager = (TableEntityManager)
		// DataObjectManagerFactory.getTableEntityManager(dataSource,
		// table);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	static JTableEx createQuerySetTable() {
		final JQuerySetTable<Map<String, Object>> table = new JQuerySetTable<Map<String, Object>>(
				null) {
			private static final long serialVersionUID = 8904977882020685565L;

			ActionCallback nullAction = new ActionCallback() {
				@Override
				public void doAction() {
					final AbstractQueryEntitySet<Map<String, Object>> qs = (AbstractQueryEntitySet<Map<String, Object>>) DataObjectManagerFactory
							.getQueryEntityManager(dataSource).query(
									new SQLValue("select id,downloads,filesize,1 as test from simple_file"));
					qs.addListener(new DataObjectQueryAdapter<Map<String, Object>>() {
						@Override
						public void next(final IDataObjectQuery<Map<String, Object>> dataObjectQuery,
								final Map<String, Object> bean, final int i, final boolean b) {
							if (bean != null) {
								bean.put("TEST", ConvertUtils.toLong(bean.get("FILESIZE")) + 1);
							}
						}
					});
					setQuerySet(qs, true);
				}

				@Override
				public boolean executeInQueue() {
					return true;
				}
			};

			@Override
			protected ActionCallback getDeleteActionCallback() {
				return nullAction;
			}

			@Override
			protected ActionCallback getEditActionCallback() {
				return nullAction;
			}

			@Override
			protected CellStyle getCellStyle(final int row, final JTableExColumn column) {
				if (row == 2) {
					final CellStyle cs = new CellStyle();
					cs.icon = SwingUtils.loadIcon("tbl-csv.png");
					cs.backgroundColor = Color.BLUE;
					return cs;
				}
				return null;
			}
		};
		// tbl_gsm_s9_asc_bts
		final AbstractQueryEntitySet qs = (AbstractQueryEntitySet) DataObjectManagerFactory
				.getQueryEntityManager(dataSource).query(
						new SQLValue("select * from tbl_gsm_s9_asc_bts"));
		qs.setFetchSize(0);
		final JTableExColumn[] columns = table.getQuerySetColumns(qs);
		columns[0].setVisible(false);
		table.setColumns(columns);
		table.setQuerySet(qs);
		return table;
	}

	public static void main(final String[] args) {
		// final java.util.Hashtable ht = UIManager.getDefaults();
		// final java.util.Enumeration e = ht.keys();
		// while (e.hasMoreElements()) {
		// System.out.println(e.nextElement());
		// }
		try {
			// UIManager.setLookAndFeel(new
			// com.sun.java.swing.plaf.windows.WindowsLookAndFeel());
			UIManager.setLookAndFeel(new com.jgoodies.looks.plastic.PlasticXPLookAndFeel());
		} catch (final Exception ex) {
		}
		final JFrameEx f = new JFrameEx();
		f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		final JTabbedPane jp = new JTabbedPane();
		jp.addTab("数据库", new JScrollPane(createQuerySetTable()));
		jp.addTab("普通", new JScrollPane(createTable()));
		f.add(jp);

		f.setSize(640, 480);
		f.setStartPosition(StartPosition.CenterInScreen);
		f.setVisible(true);
	}

	static JTableEx createTable() {
		final Random r = new Random();
		final JTableExColumn c2 = new JTableExColumn("C2");
		c2.setFreezable(true);
		c2.setColumnText("DDD\rEEE");
		final JTableExColumn c3 = new JTableExColumn("C3");
		// c3.setFreezable(true);
		final JTableExColumn c4 = new JTableExColumn("C4");
		// c4.setFreezable(true);
		final JTableExColumn c5 = new JTableExColumn("C5");
		c5.setFreezable(true);
		c5.setColumnWidth(300);
		c3.setFormat("yyyy-MM-dd");
		final JTableEx table = new JTableEx() {
			private static final long serialVersionUID = 4543815179383257555L;

			@Override
			protected CellStyle getCellStyle(final int row, final JTableExColumn column) {
				final CellStyle cellStyle = new CellStyle();
				final int col = column.getOriginalIndex();
				if (col == 4) {
					cellStyle.renderComponent = ECellRenderComponent.percent;
				}
				// if (col == 0) {
				// cellStyle.renderComponent =
				// ECellRenderComponent.multipleLine;
				// }
				return cellStyle;
			}
		};
		table.setColumns(new Object[] { "C1", c2, c3, c4, c5 });

		// table.setRowHeight(40);
		// table.addRow(new Object[] { "Row_1_\r\n", "Row_2_", null, 2, 0.3, (1f
		// /
		// 0f) });
		for (int i = 0; i < 50; i++) {
			table.addRow(new Object[] {
					i < 25 ? "Row_1_" + i : i,
					"Row_2_" + i,
					ConvertUtils.toDate("2010-" + (r.nextInt(12) + 1) + "-" + (r.nextInt(30) + 1),
							"yyyy-MM-dd"), i, (i / 100.0) });
		}
		return table;
	}
}
