package net.simpleframework.swing;

import java.awt.Component;
import java.awt.Insets;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import org.jdesktop.swingx.JXTree;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public class JTreeEx extends JXTree {
	private static final long serialVersionUID = 4083210101766754436L;

	{
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(final KeyEvent e) {
				final int code = e.getKeyCode();
				if (code == KeyEvent.VK_R) {
					refreshTreeData();
				}
			}
		});
		setCellRenderer(new DelegatingRenderer(new DefaultTreeCellRenderer() {
			private static final long serialVersionUID = 6572102096780002900L;

			@Override
			public Component getTreeCellRendererComponent(final JTree tree, final Object value,
					final boolean selected, final boolean expanded, final boolean leaf, final int row,
					final boolean hasFocus) {
				final Component comp = super.getTreeCellRendererComponent(tree, value, selected,
						expanded, leaf, row, hasFocus);
				return comp;
			}
		}));
	}

	public void refreshTreeData() {
	}

	final static Insets insets = new Insets(2, 2, 2, 2);

	@Override
	public Insets getInsets() {
		return insets;
	}
}
