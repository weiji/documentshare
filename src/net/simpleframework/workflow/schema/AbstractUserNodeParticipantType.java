package net.simpleframework.workflow.schema;

import net.simpleframework.workflow.schema.RelativeJobEnum.ERef;
import net.simpleframework.workflow.schema.RelativeJobEnum.ERelation;
import net.simpleframework.workflow.schema.RelativeJobEnum.EScope;

import org.dom4j.Element;

/**
 * 这是一个开源的软件，请在LGPLv3下合法使用、修改或重新发布。
 * 
 * @author 陈侃(cknet@126.com, 13910090885)
 *         http://code.google.com/p/simpleframework/
 *         http://www.simpleframework.net
 */
public abstract class AbstractUserNodeParticipantType extends AbstractParticipantType {

	public AbstractUserNodeParticipantType(final Element dom4jElement, final AbstractNode parent) {
		super(dom4jElement, parent);
	}

	public static class User extends AbstractUserNodeParticipantType {
		public User(final Element element, final AbstractNode parent) {
			super(element == null ? addParticipant(parent, "user") : element, parent);
		}
	}

	public static class Job extends AbstractUserNodeParticipantType {
		private boolean manual;

		public Job(final Element element, final AbstractNode parent) {
			super(element == null ? addParticipant(parent, "job") : element, parent);
		}

		public boolean isManual() {
			return manual;
		}

		public void setManual(final boolean manual) {
			this.manual = manual;
		}
	}

	public static class Chart extends AbstractUserNodeParticipantType {

		public Chart(final Element element, final AbstractNode parent) {
			super(element == null ? addParticipant(parent, "chart") : element, parent);
		}
	}

	public static class RelativeJob extends AbstractUserNodeParticipantType {
		private ERef ref;

		private ERelation relation;

		private EScope scope;

		private String preActivityName;

		public RelativeJob(final Element dom4jElement, final AbstractNode parent) {
			super(dom4jElement, parent);
		}

		public ERef getRef() {
			return ref;
		}

		public void setRef(final ERef ref) {
			this.ref = ref;
		}

		public ERelation getRelation() {
			return relation;
		}

		public void setRelation(final ERelation relation) {
			this.relation = relation;
		}

		public EScope getScope() {
			return scope;
		}

		public void setScope(final EScope scope) {
			this.scope = scope;
		}

		public String getPreActivityName() {
			return preActivityName;
		}

		public void setPreActivityName(final String preActivityName) {
			this.preActivityName = preActivityName;
		}
	}

	public static class Handle extends AbstractUserNodeParticipantType {

		public Handle(final Element element, final AbstractNode parent) {
			super(element == null ? addParticipant(parent, "handle") : element, parent);
		}
	}
}
