<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.organization.IUser"%>
<%@ page import="net.simpleframework.organization.account.IAccount"%>
<%@ page import="net.simpleframework.organization.OrgUtils"%>
<%@ page import="net.simpleframework.organization.account.Exp"%>
<%@ page
	import="net.simpleframework.organization.component.userpager.UserPagerUtils"%>
<%@ page
	import="net.simpleframework.organization.account.AccountContext"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.my.space.MySpaceUtils"%>
<%@ page import="net.simpleframework.my.space.SpaceStatBean"%><%@page
	import="net.documentshare.docu.DocuLogBean"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.docu.DocuUserBean"%><%@page
	import="net.simpleframework.util.IoUtils"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IAccount account = MySpaceUtils.getAccountAware().getAccount(requestResponse);
	if (account == null) {
		return;
	}
	final IUser user = account.user();
	if (user == null) {
		return;
	}
	final boolean me = MySpaceUtils.getAccountAware().isMyAccount(requestResponse);
	final SpaceStatBean stat = MySpaceUtils.getSpaceStat(requestResponse);
	final Object id = user.getId();
%>
<div class="space_user_layout">
	<div class="f2"><%=user.getText()%></div>
	<jsp:include page="user_nav_tooltip.jsp">
		<jsp:param value="<%=id%>" name="userId" />
	</jsp:include>
	<div class="attention">
		<table style="width: 180px; height: 40px;" cellpadding="4">
			<tr>
				<td>
					<div class="lbl">
						<a class="a2"
							onclick="$Actions['windowSpaceUserAttention']('userId=<%=id%>');"><%=stat.getAttentions()%></a>
						<span>#(space_user_layout.0)</span>
					</div>
				</td>
				<td>
					<div class="lbl">
						<a class="a2"
							onclick="$Actions['windowSpaceUserAttention']('attentionId=<%=id%>');"><%=stat.getFans()%></a>
						<span>#(space_user_layout.1)</span>
					</div>
				</td>
				<td>
					<div class="lbl2">
						<label><%=stat.getViews()%></label>
						<span>#(space_user_layout.2)</span>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<%
		DocuUserBean userBean = DocuUtils.applicationModule.getBeanByExp(DocuUserBean.class, "userId=?", new Object[] { id });
		if (userBean != null) {
	%>
	<div class="attention" style="border-top: 0px;">
		<table style="width: 180px; height: 40px;" cellpadding="4">
			<tr>
				<td>
					<div class="lbl">
						<label><%=userBean.getUpFiles()%></label>
						<span>上传</span>
					</div>
				</td>
				<td>
					<div class="lbl">
						<label><%=userBean.getDownFiles()%></label>
						<span>下载</span>
					</div>
				</td>
				<td>
					<div class="lbl2">
						<label></label>
						<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
	<div class="attr">
		<table cellpadding="1" cellspacing="1">
			<tr>
				<td style="width: 65px;">
					#(user_account_stat.11)
				</td>
				<td>
					<%
						if (me) {
							out.write("<a class=\"a2\" onclick=\"$Actions['userAccountLogWindow']();\">");
						}
						out.write(String.valueOf(account.getExp()));
						if (me) {
							out.write("</a>");
						}
						final Exp exp = AccountContext.getExp(account.getExp());
						if (exp != null) {
							out.write(UserPagerUtils.getExpIcon(exp));
						}
					%>
				</td>
			</tr>
			<tr>
				<td>
					#(user_account_stat.8)
				</td>
				<td>
					<%
						if (me) {
							out.write("<a class=\"a2\" onclick=\"$Actions['userAccountLogWindow']();\">");
						}
						out.write(String.valueOf(account.getPoints()));
						if (me) {
							out.write("</a>");
						}
					%>
				</td>
			</tr>
			<tr>
				<td>
					#(space_user_layout.3)
				</td>
				<td><%=ConvertUtils.toDateString(account.getCreateDate())%></td>
			</tr>
			<tr>
				<td>
					#(user_edit_base.8)
				</td>
				<td><%=StringUtils.blank(user.getHometown())%></td>
			</tr>
		</table>
	</div>
	<%
		if (me) {
	%>
	<div class="edit_btn">
		<a class="a2"
			onclick="$Actions['editUserWindow']('<%=OrgUtils.um().getUserIdParameterName()%>=<%=id%>');">#(space_user_layout.4)</a>
	</div>
	<%
		}
	%>
</div>