<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.content.bbs.BbsUtils"%>
<%@ page import="net.simpleframework.util.DateUtils.TimeDistance"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<% 
	final String bbs_layout = BbsUtils.deployPath
			+ "jsp/bbs_portal.jsp";
	final PageRequestResponse requestResponse = new PageRequestResponse(
			request, response);
%>
<div class="block_layout1">
<div class="t f4">一周点击排行</div>
<div class="wrap_text">
<%
	request.setAttribute("__qs", BbsUtils.queryTopics(requestResponse,
			null, null, false, TimeDistance.week, null, 1));
%> <jsp:include page="<%=bbs_layout%>" flush="true">
	<jsp:param value="false" name="showMore" />
	<jsp:param value="dot1" name="dot" />
</jsp:include></div>
</div>

<div class="block_layout1">
<div class="t f4">一月点击排行</div>
<div class="wrap_text">
<%
	request.setAttribute("__qs", BbsUtils.queryTopics(requestResponse,
			null, null, false, TimeDistance.month, null, 1));
%> <jsp:include page="<%=bbs_layout%>" flush="true">
	<jsp:param value="false" name="showMore" />
	<jsp:param value="dot1" name="dot" />
</jsp:include></div>
</div>

<div class="block_layout1">
<div class="t f4">精华主题</div>
<div class="wrap_text">
<%
	request.setAttribute("__qs", BbsUtils.queryTopics(requestResponse,
			null, null, true, null, null, 0));
%> <jsp:include page="<%=bbs_layout%>" flush="true">
	<jsp:param value="false" name="showMore" />
	<jsp:param value="dot2" name="dot" />
</jsp:include></div>
</div>