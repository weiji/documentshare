<%@page import="net.simpleframework.content.news.NewsUtils"%>
<%@page
	import="net.simpleframework.web.page.component.ComponentParameter"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.ConvertUtils"%>
<%@ page import="net.simpleframework.content.ContentLayoutUtils"%><%@page
	import="net.simpleframework.core.ado.IDataObjectQuery"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.simpleframework.util.DateUtils"%><%@page
	import="net.simpleframework.content.ContentUtils"%><%@page
	import="net.simpleframework.organization.account.Account"%><%@page
	import="net.documentshare.docu.DocuUserBean"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IDataObjectQuery<?> qs = DocuUtils.queryDocu(requestResponse, null,"docu", "userPoint");
%>
<div class="list_layout">
	<div class="rrow" style="padding-left: 2px;">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<th>
					用户
				</th>
				<th width="60" align="center">
					积分
				</th>
				<th width="60" align="center">
					文档数
				</th>
			</tr>
		</table>
	</div>
	<%
		Account account;
		while ((account = (Account) qs.next()) != null) {
			if (qs.position() >= 10) {
				break;
			}
	%>
	<div class="rrow" style="padding-left: 2px;">
		<table width="100%" cellpadding="0" cellspacing="0">
			<tr>
				<td><%=qs.position()%>.<%=ContentUtils.getAccountAware().wrapAccountHref(requestResponse, account.user())%></td>
				<td class="nnum" width="60" align="center">
					<%=account.getPoints()%>
				</td>
				<td class="nnum" width="60" align="center">
					<%
						final DocuUserBean userBean = DocuUtils.applicationModule.getBeanByExp(DocuUserBean.class, "userId=?",
									new Object[] { account.getId() });
							out.println(userBean == null ? 0 : userBean.getUpFiles());
					%>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
</div>