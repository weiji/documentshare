<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.content.blog.BlogUtils"%>
<%
	final String blog_view_c = BlogUtils.deployPath
			+ "jsp/blog_view_c.jsp";
	final String blog_view_lr = BlogUtils.deployPath
			+ "jsp/blog_view_lr.jsp";
%>
<div class="c_left">
	<jsp:include page="<%=blog_view_c%>"></jsp:include>
</div>
<div class="c_right">
	<jsp:include page="<%=blog_view_lr%>"></jsp:include>
</div>
<script type="text/javascript">
	(function() {
		$Comp.fixMaxWidth(".newspager_template .inherit_c", 680);
	})();
</script>