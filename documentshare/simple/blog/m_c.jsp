<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.content.blog.BlogUtils"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.content.EContentType"%>
<%@ page import="net.simpleframework.util.DateUtils.TimeDistance"%>
<%
	final String blog_home = BlogUtils.deployPath + "jsp/blog_home.jsp";
	final String blog_layout = BlogUtils.deployPath
			+ "jsp/blog_portal.jsp";
	final String blog_remark_layout = BlogUtils.deployPath
			+ "jsp/blog_remark_layout.jsp";
	final PageRequestResponse requestResponse = new PageRequestResponse(
			request, response);
%>
<div class="c_left"><jsp:include page="<%=blog_home%>"></jsp:include>
</div>
<div class="c_right">
	<div class="block_layout1">
		<div class="t f4">推荐主题</div>
		<div class="wrap_text">
			<%
				request.setAttribute("__qs", BlogUtils.queryBlogs(requestResponse,
						EContentType.recommended, null, false, 0));
			%>
			<jsp:include page="<%=blog_layout%>" flush="true">
				<jsp:param value="false" name="showMore" />
				<jsp:param value="dot1" name="dot" />
			</jsp:include></div>
	</div>

	<div class="block_layout1">
		<div class="t f4">一月点击排行</div>
		<div class="wrap_text">
			<%
				request.setAttribute("__qs", BlogUtils.queryBlogs(requestResponse,
						null, TimeDistance.month, false, 1));
			%>
			<jsp:include page="<%=blog_layout%>" flush="true">
				<jsp:param value="false" name="showMore" />
				<jsp:param value="dot2" name="dot" />
			</jsp:include></div>
	</div>

	<div class="block_layout1">
		<div class="t f4">最新评论</div>
		<div class="wrap_text">
			<%
				request.setAttribute("__qs",
						BlogUtils.queryRemarks(requestResponse));
			%>
			<jsp:include page="<%=blog_remark_layout%>" flush="true">
				<jsp:param value="false" name="showMore" />
				<jsp:param value="dot2" name="dot" />
			</jsp:include></div>
	</div>

	<div class="block_layout1">
		<div class="t f4">活跃用户</div>
		<div class="wrap_text"></div>
	</div>
</div>
