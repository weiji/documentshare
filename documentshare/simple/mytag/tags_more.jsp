<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.core.ado.IDataObjectQuery"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.mytag.MyTagUtils"%><%@page
	import="net.documentshare.mytag.MyTagBean"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IDataObjectQuery<MyTagBean> qs = MyTagUtils.layoutTags(requestResponse);
%>
<div class="tag_layout simple_toolbar">
	<%
		MyTagBean tag;
		while ((tag = qs.next()) != null) {
	%><a
		title="#(tags_layout.3): <%=tag.getFrequency()%>, #(tags_layout.4): <%=tag.getViews()%>"
		href="<%=MyTagUtils.applicationModule.getTagUrl(requestResponse, tag)%>"><%=tag.getTagText()%></a>
	<%
		}
	%>
</div>