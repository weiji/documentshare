<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.core.ado.IDataObjectQuery"%>
<%@page
	import="net.simpleframework.web.page.component.ado.IDbComponentHandle"%>
<%@page import="net.documentshare.mytag.MyTagBean"%>
<%@page import="net.documentshare.mytag.MyTagUtils"%>
<%@page import="net.documentshare.mytag.IMyTagApplicationModule"%><%@page
	import="net.simpleframework.content.IContentPagerHandle"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final IDataObjectQuery<MyTagBean> qs = MyTagUtils.layoutTags(requestResponse);
	final StringBuilder params = new StringBuilder();
	params.append(IMyTagApplicationModule._CATALOG_ID).append("=").append(request.getParameter(IMyTagApplicationModule._CATALOG_ID))
			.append("&").append(IContentPagerHandle._VTYPE).append("=").append(request.getParameter(IContentPagerHandle._VTYPE));
%>
<div class="tag_layout">
	<%
		int i = -1;
		MyTagBean tag;
		while ((tag = qs.next()) != null) {
			if (++i == 7) {
				i = 0;
			}
	%><a
		title="#(tags_layout.3): <%=tag.getFrequency()%>, #(tags_layout.4): <%=tag.getViews()%>"
		style="font-size: <%=9 + i%>px; font-family: <%=MyTagUtils.fontWeight[i]%>"
		href="<%=MyTagUtils.applicationModule.getTagUrl(requestResponse, tag)%>"><%=tag.getTagText()%></a>
	<%
		}
	%><a class="a2"
		onclick="$Actions['tagsMoreWindow']('<%=params.toString()%>&rows=0');">#(tags_layout.1)</a>
	<%
		if (IDbComponentHandle.Utils.isManager(requestResponse, MyTagUtils.applicationModule.getManager(requestResponse))) {
	%><a class="a2"
		onclick="$Actions['tagsManagerWindow']('<%=params.toString()%>');">#(Edit)</a>
	<%
		}
	%>
</div>
<style type="text/css">
.tag_layout a {
	margin: 2px 4px;
	display: inline-block;
}
</style>