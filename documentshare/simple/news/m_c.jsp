<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.util.DateUtils.TimeDistance"%>
<%@ page import="net.simpleframework.content.EContentType"%>
<%@ page import="net.simpleframework.applets.tag.TagUtils"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.content.news.NewsUtils"%>
<%@ page import="net.simpleframework.web.EFunctionModule"%><%@page
	import="net.simpleframework.content.IContentPagerHandle"%>
<%
	final String news = NewsUtils.deployPath + "jsp/news.jsp";
	final String news_layout = NewsUtils.deployPath + "jsp/news_layout.jsp";
	final String news_remark_layout = NewsUtils.deployPath + "jsp/news_remark_layout.jsp";
	final String tags_params = IContentPagerHandle._VTYPE + "=" + EFunctionModule.news.ordinal();
	final String tags_layout = TagUtils.deployPath + "jsp/tags_layout.jsp?" + tags_params;
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<div class="c_left"><jsp:include flush="true" page="<%=news%>"></jsp:include></div>
<div class="c_right">
	<div class="block_layout2">
		<div class="t f4">
			热门标签
		</div>
		<div class="c"><jsp:include page="<%=tags_layout%>" flush="true">
				<jsp:param value="50" name="rows" />
			</jsp:include></div>
	</div>

	<div class="block_layout1">
		<div class="t f4">
			推荐资讯
		</div>
		<div class="wrap_text">
			<%
				request.setAttribute("__qs", NewsUtils.queryNews(requestResponse, null, EContentType.recommended, null, 0));
			%>
			<jsp:include page="<%=news_layout%>" flush="true">
				<jsp:param value="false" name="showMore" />
				<jsp:param value="dot1" name="dot" />
			</jsp:include></div>
	</div>

	<div class="block_layout1">
		<div class="t f4">
			一月点击排行
		</div>
		<div class="wrap_text">
			<%
				request.setAttribute("__qs", NewsUtils.queryNews(requestResponse, null, null, TimeDistance.month, 1));
			%>
			<jsp:include page="<%=news_layout%>" flush="true">
				<jsp:param value="false" name="showMore" />
				<jsp:param value="dot2" name="dot" />
			</jsp:include></div>
	</div>

	<div class="block_layout1">
		<div class="t f4">
			最新评论
		</div>
		<div class="wrap_text"><jsp:include
				page="<%=news_remark_layout%>" flush="true"></jsp:include></div>
	</div>
</div>

