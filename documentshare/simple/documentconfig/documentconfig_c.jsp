<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.documentshare.documentconfig.DocumentConfigMgr"%><%@page
	import="net.documentshare.documentconfig.LocalStorageBean"%><%@page
	import="net.documentshare.documentconfig.FtpStorageBean"%><%@page
	import="net.simpleframework.util.IoUtils"%><%@page
	import="java.io.File"%><%@page
	import="net.documentshare.documentconfig.StorageBean"%><%@page
	import="org.apache.tools.ant.types.resources.selectors.InstanceOf"%>

<%
	final DocumentConfigMgr docuMgr = DocumentConfigMgr.getDocuMgr();
%>
<style>
.listpager .titem {
	display: inline-block;
	vertical-align: top;
	border: 1px solid #EEE;
	margin-top: -1px;
	width: 99%;
}

.iconpager .titem {
	display: inline-block;
	vertical-align: top;
	border: 1px solid #EEE;
	margin-top: -1px;
	width: 49%;
}

.listpager .titem:HOVER,.iconpager .titem:HOVER {
	background-color: #F8F8F8;
}

.listpager .title,.iconpager .title {
	font-weight: bold;
}
</style>
<div class="simple_toolbar1" id="documentconfigForm">
	<table>
		<tr>
			<td>
				RAR安装路径：
			</td>
			<td>
				<input type="text" id="rar" name="rar" style="width: 500px;"
					value="<%=docuMgr.getValue("rar")%>">
				<input type="button" value="保存路径" onclick="$IT.A('valueSaveAct');">
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<table class="listpager">
					<tr>
						<td>
						</td>
						<td>
							<input type="button" onclick="$IT.A('storageLocalWindowAct');"
								value="添加磁盘目录">
							<input type="button" onclick="$IT.A('storageFtpWindowAct');"
								value="添加服务器目录">
						</td>
					</tr>
					<%
						for (final StorageBean storageBean : docuMgr.getStorageList()) {
							final boolean current = storageBean.getId().equals(docuMgr.getStorageBean().getId());
					%>
					<tr>
						<td>
							磁盘路径<%=storageBean.getId()%>：
						</td>
						<td class="titem" rowid="<%=storageBean.getId()%>"
							type="<%=storageBean.getType()%>">
							<table>
								<tr>
									<td valign="top">
										<img style="width: 60px; height: 60px;"
											src="/simple/documentconfig/<%=current ? "run" : "normal"%>.png">
									</td>
									<td>
										<table style="width: 100%;">
											<tr>
												<td>
													<a class="dcm down_menu_image" href="javascript:void(0);"></a>
												</td>
											</tr>
											<tr>
												<td><%=storageBean%></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<%
						}
					%>
				</table>
			</td>
		</tr>
	</table>
</div>
<script type="text/javascript">
$ready(function() {
	var dcm = $Actions["documentconfigMenu"];
	if (dcm)
		dcm.bindEvent(".dcm");
});
</script>