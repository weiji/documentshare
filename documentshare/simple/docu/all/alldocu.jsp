<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.content.EContentStatus"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<div id="documentNav"><%=DocuUtils.documentManagerNav(requestResponse)%></div>
<jsp:include page="/simple/docu/all/alldocu_c.jsp" flush="true"></jsp:include>
<style>
.displayOut {
	color: blue;
}
</style>
<script type="text/javascript">
$ready(function() {
	$Actions['allDocuTableAct']('docu_type=<%=EDocuFunction.docu.name()%> ');
});
</script>