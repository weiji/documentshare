<%@page import="net.simpleframework.organization.OrgUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	String userId = request.getParameter(OrgUtils.um().getUserIdParameterName());
%>
<div class="simple_tabs_content space_tabs_content">
	<div id="documentNav">
		<a href="javascript:void(0);" class="nav_arrow a2"
			onclick="refreshDocu(this);$Actions['myCorpusListTableAct']('type=my&userid=<%=userId%>');"
			id="myAll">我的文辑</a><span style="margin: 0px 4px;">|</span><a
			href="javascript:void(0);"
			onclick="$Actions['updateCorpusWindow']();" class="" id="myUpload">创建文辑</a><span
			style="margin: 0px 4px;">|</span><a href="javascript:void(0);"
			onclick="refreshDocu(this);$Actions['myCorpusListTableAct']('type=collect&userid=<%=userId%>');"
			id="myNotEdit">我的收藏</a>
	</div>
	<jsp:include page="mycorpus_list_c.jsp" flush="true"></jsp:include>
</div>