<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.mytag.ETagType"%><%@page
	import="net.documentshare.mytag.MyTagUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%><%@page
	import="net.simpleframework.content.IContentPagerHandle"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String tags_params = IContentPagerHandle._VTYPE + "=" + ETagType.docu.ordinal();
	final String tags_layout = MyTagUtils.deploy + "/tags_layout.jsp?" + tags_params + "&s="
			+ StringUtils.text(requestResponse.getRequestParameter("s"), "docu") + "&od="
			+ StringUtils.text(requestResponse.getRequestParameter("od"), "new");
%>
<div class="block_layout1">
	<div class="wrap_text">
		<img src="/simple/docu/images/upload.png" style="cursor: pointer;"
			onclick="$Actions.loc('/space.html?t=document&id=myUpload');"
			alt="我要上传">
	</div>
</div>
<div class="block_layout2">
	<div class="t f4">
		热门标签
	</div>
	<div class="wrap_text"><jsp:include page="<%=tags_layout%>"
			flush="true">
			<jsp:param value="50" name="rows" />
		</jsp:include></div>
</div>
<div class="block_layout1">
	<div class="t f4">
		热门下载
	</div>
	<div class="wrap_text">
		<jsp:include page="download_tabs.jsp" flush="true"></jsp:include>
	</div>
</div>
<div class="block_layout1">
	<div class="t f4">
		最新上传
	</div>
	<div class="wrap_text">
		<jsp:include page="docu_list_show.jsp" flush="true">
			<jsp:param value="new" name="type" />
			<jsp:param value="dot1" name="dot" />
		</jsp:include>
	</div>
</div>
<div class="block_layout1">
	<div class="t f4">
		评分最高
	</div>
	<div class="wrap_text">
		<jsp:include page="docu_list_show.jsp" flush="true">
			<jsp:param value="grade" name="type" />
			<jsp:param value="dot1" name="dot" />
		</jsp:include>
	</div>
</div>
<div class="block_layout1">
	<div class="t f4">
		用户积分排行榜
	</div>
	<div class="wrap_text">
		<jsp:include page="user_list_point.jsp" flush="true">
			<jsp:param value="dot1" name="dot" />
		</jsp:include>
	</div>
</div>