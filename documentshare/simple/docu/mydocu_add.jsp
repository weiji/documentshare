<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="net.documentshare.docu.DocuUtils"%>


<div id="docuForm">
	<input type="hidden" id="docuId" name="docuId">
	<table width="100%'">
		<tbody>
			<tr>
				<td class="l">
					标题：
				</td>
				<td>
					<input type="text" id="docu_title" name="docu_title"
						style="width: 100%" value="">
				</td>
			</tr>
			<tr>
				<td class="l" valign="top">
					描述：
				</td>
				<td>
					<textarea id="docu_content" name="docu_content" rows="5"
						style="width: 99%"></textarea>
				</td>
			</tr>
			<tr>
				<td class="l">
					关键字：
				</td>
				<td>
					<input type="text" id="docu_keyworks" name="docu_keyworks"
						style="width: 100%">
					<div id="docu_tag"></div>
				</td>
			</tr>
			<tr>
				<td class="l">
					文档分类：
				</td>
				<td>
					<div id="td_docu_catalog" style="width: 100%"></div>
					<input type="hidden" name="docu_catalog" id="docu_catalog">
				</td>
			</tr>
			<tr style="display: none;" class="code_catalog_id">
				<td class="l">
					编译环境分类：
				</td>
				<td>
					<div id="td_docu_code_catalog" style="width: 100%"></div>
					<input type="hidden" name="docu_code_catalog"
						id="docu_code_catalog">
				</td>
			</tr>
			<tr style="display: none;" class="code_catalog_id">
				<td class="l">
					语言：
				</td>
				<td>
					<select id="code_language" name="code_language">
						<%
							for (String l : DocuUtils.languages) {
						%>
						<option value="<%=l%>"><%=l%></option>
						<%
							}
						%>
					</select>
				</td>
			</tr>
			<tr>
				<td class="l">
					属性：
				</td>
				<td>
					<select id="docu_function" name="docu_function"
						onchange="selectDocuAttr(this);">
						<%
							for (EDocuFunction docu : EDocuFunction.values()) {
						%>
						<option value="<%=docu.name()%>"><%=docu.toString()%></option>
						<%
							}
						%>
					</select>
				</td>
			</tr>
			<tr class="docu_pages">
				<td class="l">
					预览页数：
				</td>
				<td>
					<input type="text" style="width: 50px" id="docu_free_page"
						name="docu_free_page" value="0">
					<span style="color: red;">0为不限制,或者输入百分比,如20%</span>
				</td>
			</tr>
			<tr>
				<td class="l">
					所需积分：
				</td>
				<td>
					<input type="text" style="width: 50px" id="docu_point"
						name="docu_point" value="0">
				</td>
			</tr>
		</tbody>
	</table>
</div>
<div align="center">
	<input type="button" class="button2" id="__docuAddBtn"
		onclick="$IT.A('docuEditSaveAct');" value="提交文档">
</div>
<style type="text/css">
#docuForm .l {
	width: 100px;
	text-align: right;
}

#docuForm {
	background: #f7f7ff;
	border: 1px solid #ddd;
	padding: 6px 8px;
}

#docuForm .ccontent {
	border-bottom: 1px dotted #ccc;
}
</style>
<script type="text/javascript">
$ready(function() {
	addTextButton("docu_catalog_text", "docuCatalogAct", "td_docu_catalog",
			false);
	addTextButton("docu_code_catalog_text", "docuCodeCatalogAct",
			"td_docu_code_catalog", false);
});
function selectDocuAttr(t) {
	if ('code' == t.value) {
		$$('.code_catalog_id').each(function(c) {
			c.style.display = '';
		});
	} else {
		$$('.code_catalog_id').each(function(c) {
			c.style.display = 'none';
		});
	}
}
</script>