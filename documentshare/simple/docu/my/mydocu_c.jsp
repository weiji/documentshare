<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%><%@page
	import="net.simpleframework.web.page.PageRequestResponse"%><%@page
	import="net.documentshare.docu.DocuUtils"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<div class="simple_tabs_content space_tabs_content">
	<div id="documentNav"><%=DocuUtils.documentNav(requestResponse)%></div>
	<div class="space_content_item">
		<div id="mydocuId"></div>
	</div>
</div>