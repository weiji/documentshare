<%@page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page
	import="net.simpleframework.web.page.component.ui.pager.PagerUtils"%>
<%@ page import="java.util.List"%><%@page
	import="net.simpleframework.util.HTMLBuilder"%><%@page
	import="net.documentshare.docu.DocuBean"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.simpleframework.util.StringUtils"%>

<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
%>
<style>
.pager .pager_top_block .pager_title {
	font-size: 11pt;
	margin-left: 3px;
}

.pager .pager_head {
	margin-right: 3px;
}

#_tablepaper .titem {
	display: inline-block;
	vertical-align: top;
	border-bottom: 1px solid #EEE;
	margin-top: -1px;
	width: 99%;
}

#_tablepaper .titem:HOVER {
	background-color: #F8F8F8;
}

#_tablepaper .title {
	font-weight: bold;
}

.pager .pager_top_block .pager_title {
	height: 20px;
}
</style>
<div id="_tablepaper">
	<%
		final List<?> data = PagerUtils.getPagerList(request);
		if (data == null && data.size() == 0) {
			return;
		}
		for (Object o : data) {
			final DocuBean docuBean = (DocuBean) o;
	%>
	<div class="titem" rowid="<%=docuBean.getId()%>">
		<table cellpadding="4" style="width: 100%">
			<tr>
				<td valign="top" width="100%">
					<table style="width: 100%">
						<tr>
							<td><%=docuBean.getTitle()%></td>
							<td align="right">
								<%
									if (ItSiteUtil.isManage(requestResponse, DocuUtils.applicationModule) && StringUtils.hasText(request.getParameter("docu_status")))
											out.println("<a class=\"myDocu down_menu_image\" href=\"javascript:void(0);\"></a>");
								%>
							</td>
						</tr>

					</table>
				</td>
			</tr>
		</table>
	</div>
	<%
		}
	%>
</div>
