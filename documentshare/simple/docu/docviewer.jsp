<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script
	src="<%=request.getContextPath()%>/scripts/lib/jquery-1.6.4.min.js">
	
</script>
<script
	src="<%=request.getContextPath()%>/scripts/lib/jquery.json.min.js">
	
</script>
<script src="<%=request.getContextPath()%>/scripts/docviewer.js">
	
</script>
<script src="<%=request.getContextPath()%>/scripts/view.js">
	
</script>
<script
	src="<%=request.getContextPath()%>/scripts/lib/swfobject/swfobject.js">
	
</script>
<script>
	$(function() {
		function getParameter(propties) {
			var re = new RegExp(propties + "=([^\&]*)", "i"), a = re
					.exec(location.search);
			if (!a)
				return null;
			return a[1];
		}
		var docViewer = new DocViewer({
			docUrl : getParameter('url'),//文档地址 于info参数选其一 info=true则忽略此参数
			'case' : DocViewer.NODE_CASE,
			width : '100%',
			height : '100%',
			renderTo : $('#flashContent'),
			docId : getParameter('id'),//文档ID
			requestDocInfo : getParameter('info')
		//是否自动请求加载文档
		});
	});
</script>
<style>
* {
	margin: 0;
	padding: 0;
}
</style>
<div id="flashContent" style="width: 100%; height: 530px;display:block"></div>
