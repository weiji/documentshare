<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@ page import="net.simpleframework.util.StringUtils"%>
<%@ page import="net.simpleframework.web.WebUtils"%>
<%@page import="net.simpleframework.web.IWebApplicationModule"%>
<%@page import="net.simpleframework.util.ConvertUtils"%><%@page
	import="net.documentshare.utils.ItSiteUtil"%><%@page
	import="net.documentshare.docu.DocuUtils"%><%@page
	import="net.documentshare.docu.EDocuFunction"%><%@page
	import="net.documentshare.docu.EDocuType1"%>
<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	final String appUrl = DocuUtils.applicationModule.getApplicationUrl(requestResponse);
	final int t = ConvertUtils.toInt(request.getParameter("t"), 0);
	final boolean manager = ItSiteUtil.isManage(requestResponse, DocuUtils.applicationModule);
	final int catalogId = ConvertUtils.toInt(request.getParameter("catalogId"), 0);
	String c1 = request.getParameter("c");
	String _docu_topic = request.getParameter("_docu_topic");
	boolean isNode = DocuUtils.isNode(catalogId);
	if(!isNode&&!manager){
		return;
	}
%>
<div id="__barHeader"></div>
<div class="simple_toolbar2">
	<table width="100%" cellpadding="1" cellspacing="0">
		<tr>
			<td width="100%" colspan="2">
				<table width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td align="left">
							<%
								if (manager) {
									out.write("<a class=\"a2\" style=\"vertical-align: middle;\" onclick=\"$Actions['docuMgrToolsWindowAct']();\">管理员工具</a>");
								}
							%>
						</td>
						<td id="__blog_sech" align="right"></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td width="100%" colspan="2">
				<div style="display: none;" id="sech_pane_params"
					class="sech_pane_params">
					<table cellpadding="2" cellspacing="0" style="width: 420px;">
						<tr>
							<td class="l">
								标题
							</td>
							<td class="v">
								<input type="text" name="_docu_topic" id="_docu_topic" />
							</td>
						</tr>
						<tr>
							<td class="l">
								属性
							</td>
							<td class="v">
								<select id="s" name="s">
									<%
										for (final EDocuFunction docu : EDocuFunction.values()) {
									%>
									<option value="<%=docu.ordinal() + 1%>"><%=docu.toString()%></option>
									<%
										}
									%>
								</select>
							</td>
						</tr>
						<tr>
							<td class="l">
								分类
							</td>
							<td class="v">
								<a id="__catalogSelectDict">添加分类</a>
								<div id="_docu_catalog"></div>
							</td>
						</tr>
						<tr>
							<td class="l"></td>
							<td class="v">
								<input type="button" class="button2" value="#(Button.Ok)"
									onclick="M_UTILS.search_submit(this);" />
								<input type="button" value="#(Button.Cancel)"
									onclick="this.up('div').$toggle();" />
							</td>
						</tr>
					</table>
				</div>
			</td>
		</tr>
		<%
			if (manager) {
		%>
		<tr>
			<td colspan="2"><%=DocuUtils.applicationModule.tabs(requestResponse)%></td>
		</tr>
		<%
			}
		%>
		<%
			if (t == EDocuType1.all.ordinal()) {
		%>
		<tr>
			<td nowrap="nowrap" align="right" colspan="2">
				<%=DocuUtils.applicationModule.tabs2(requestResponse)%>
			</td>
		</tr>
		<%
		if(!StringUtils.hasText(_docu_topic)&&!StringUtils.hasText(c1)){
		%>
		<tr>
			<td nowrap="nowrap" align="right" colspan="2">
				<%=DocuUtils.applicationModule.tabs13(requestResponse)%>
			</td>
		</tr>
		<%
			}}
		%>
	</table>
</div>
<script type="text/javascript">

var M_UTILS = {
	search_submit : function(obj) {
		var sp = obj.up('.sech_pane_params');
		var catalog = "_docu_catalog=";
		var i = 0;
		$$("#_docu_catalog div[id]").each(function(d) {
			if (i++ > 0)
				catalog += ";";
			catalog += d.id;
		});
		var params = $$Form(sp).addParameter(catalog);
		$Actions.loc('/docu.html?' + params);
	},
	insert_forum : function(selects) {
		if (!selects.any(function(node) {
			return node.selected;
		})) {
			return;
		}
		var c = $("_docu_catalog");
		selects.each(function(node) {
			if (!node.selected || c.down("#" + node.id)) {
				return;
			}
			c.insert(new Element("div", {
				id : node.id
			}).insert(new Element("span").update(node.text)).insert(
					new Element("a", {
						className : "delete_image",
						onclick : "this.up().remove();"
					})));
		});
		return true;
	}
};
</script>
<script type="text/javascript">
(function() {
		var sb = $Comp.searchButton(function(sp) {
			$Actions.loc("<%=appUrl%>".addParameter("t=0&c=" + $F(sp.down(".txt"))));
		}, function(sp) {
			$('sech_pane_params').$toggle();
		}, '当前已有<%=DocuUtils.docuCounter%>份文档', 210);
		$("__blog_sech").update(sb);
		<%final String c = WebUtils.toLocaleString(request.getParameter("c"));
			if (StringUtils.hasText(c)) {%>
			sb.down(".txt").setValue("<%=c%>");
		<%}%>
	}
	)();
$IT.bindJS("#__catalogSelectDict", true);
</script>