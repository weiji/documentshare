<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="net.simpleframework.web.page.PageRequestResponse"%>
<%@page import="net.simpleframework.organization.account.AccountSession"%>
<%@page import="net.simpleframework.organization.account.IAccount"%><%@page
	import="net.documentshare.question.QuestionUtils"%>


<%
	final PageRequestResponse requestResponse = new PageRequestResponse(request, response);
	IAccount account = AccountSession.getLogin(session);
	long total = account.getPoints();
	long left = QuestionUtils.queryQuestionPoints(requestResponse);
%>
<div>
	你目前还有<%=total%>积分,发布的问答占去<%=left%>积分,所以你最高只允许填<%=total - left%>积分
</div>
